
// returns false if its not the solution
// returns the two strings so i can visually compare
fn has_edit_dist_one(a: &str, b: &str) -> Option<String> {
    let mut found_one: bool = false;
    if a.chars().zip(b.chars()).map(|(c_a, c_b)| if c_a == c_b {0} else {1}).sum::<i32>() == 1i32 {
        Some(String::from(a.to_owned() + "\n" + b))
    }
    else {
        None
    }
}

fn main(){
    let keys: Vec<&'static str> = include_str!("input.txt")
                                  .lines()
                                  .collect();
    for i in 0..keys.len(){
        for j in i+1..keys.len(){
            if let Some(ans) = has_edit_dist_one(keys[i], keys[j]) {
                println!("{}", ans);
                break;
            }
        }
    }
}