use std::collections::HashMap;

fn convert_to_counts(key: &str) -> HashMap<char, u32> {
    let mut out = HashMap::new();
    for ch in key.chars() {
        *out.entry(ch).or_insert(0u32) += 1;
    }
    out
}

fn has_two_or_three_count(counts: &HashMap<char, u32>) -> (bool, bool) {
    let mut two = false;
    let mut three = false;
    for &val in counts.values() {
        if val == 2 {
            two = true;
        }
        if val == 3 {
            three = true;
        }
    }
    (two, three)
}
fn main() {
    let counts_list: Vec<HashMap<char, u32>> = 
            include_str!("input.txt")
            .lines()
            .map(|key| convert_to_counts(key))
            .collect();
    let mut two_count = 0;
    let mut three_count = 0;
    for map in counts_list.iter() {
        let (two, three) = has_two_or_three_count(map);
        if two {
            two_count += 1
        }
        if three {
            three_count += 1
        }
    }
    println!("Checksum is {}", two_count*three_count);
}