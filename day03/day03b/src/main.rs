extern crate regex;

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate itertools;

use regex::Regex;


// Represents a rectangle from top-left corner to bottom-right corner, inclusive
struct Rectangle {
    idx: u32,
    start_y: u32,
    start_x: u32,
    end_y: u32,
    end_x: u32
}

impl Rectangle {
    fn contains(&self, y: u32, x: u32) -> bool {
        y >= self.start_y &&
        x >= self.start_x &&
        y <= self.end_y &&
        x <= self.end_x
    }
    fn get_points(&self) -> impl Iterator<Item=(u32,u32)> {
        iproduct!((self.start_y..=self.end_y), (self.start_x..=self.end_x))
    }
    fn any_overlaps(&self, others: &[Self]) -> bool {
        self.get_points()
            .any(|(y,x)| num_overlaps(y, x, &others) > 1)  // Assume self is in list of others, so one overlap is ok
    }
}

impl<'a> From<&'a str> for Rectangle {
    // Use regex to parse this from a line in the input file
    fn from(line: &'a str) -> Self {
        // Only compile this regex once
        lazy_static! {
            //example "#1259 @ 567,491: 21x12"
            static ref RE: Regex = Regex::new(r"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
        }
        let caps = RE.captures(line).unwrap();
        let idx = caps.get(1).unwrap().as_str().parse::<u32>().unwrap();
        let start_x = caps.get(2).unwrap().as_str().parse::<u32>().unwrap();
        let start_y = caps.get(3).unwrap().as_str().parse::<u32>().unwrap();
        let delta_x = caps.get(4).unwrap().as_str().parse::<u32>().unwrap() - 1;
        let delta_y = caps.get(5).unwrap().as_str().parse::<u32>().unwrap() - 1;
        Rectangle { idx: idx,
                    start_y: start_y,
                    start_x: start_x,
                    end_y: start_y + delta_y,
                    end_x: start_x + delta_x}
    }
}

fn num_overlaps(y: u32, x: u32, rectangles: &[Rectangle]) -> usize {
    rectangles.iter()
              .filter(|r| r.contains(y,x))
              .count()
}

fn main() {
    let rectangles: Vec<Rectangle> = include_str!("input.txt")
                                     .lines()
                                     .map(|s| s.into())
                                     .collect();
    let out = rectangles.iter()
              .filter(|r| !r.any_overlaps(&rectangles))
              .next()
              .unwrap()
              .idx;

    println!("{}", out);
}
