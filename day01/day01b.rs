use std::collections::HashSet;

fn main() {
    let parsed_iter = include_str!("input.txt")
                      .lines()
                      .map(|num| num.parse::<i32>())
                      .filter_map(|x| x.ok())
                      .cycle();  // Loop forever
    let mut seen_frequencies = HashSet::new();
    let mut cur = 0i32;
    seen_frequencies.insert(cur);
    for increment in parsed_iter {
        cur += increment;
        if !seen_frequencies.insert(cur) {
            break;
        }
    }
    println!("{:?}", cur);

}