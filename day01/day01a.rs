fn main() {
    let output = include_str!("input.txt")
                 .lines()
                 .map(|num| num.parse::<i32>())
                 .filter_map(|x| x.ok())
                 .sum::<i32>();
    println!("{}", output);
}