use regex::Regex;
use std::collections::HashMap;
use std::fmt;

#[macro_use]
extern crate lazy_static;

struct Hour {
    guard: u32,
    sleep: [bool; 60]
}

impl fmt::Debug for Hour {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: ", self.guard)?;
        for i in 0..self.sleep.len() {
            write!(f, "{}", if self.sleep[i] {1}  else {0})?;
        }
        Ok(())
    }
}

#[derive(Debug)]
enum Event {
    Begin(u32),
    Sleep(usize),
    Wake(usize)
}

fn parse_line(line: &str) -> Event {
    // [1518-08-02 00:02] Guard #1223 begins shift
    // [1518-06-11 00:46] wakes up
    // [1518-05-29 00:51] falls asleep
    lazy_static! {
        static ref BEGIN_RE: Regex = Regex::new(
            r"\[(\d{4}-\d{2})-\d{2} \d{2}:\d{2}\] Guard #(\d+) begins shift")
            .unwrap();
        static ref WAKE_RE: Regex = Regex::new(
            r"\[\d{4}-\d{2}-\d{2} \d{2}:(\d{2})\] wakes up")
            .unwrap();
        static ref SLEEP_RE: Regex = Regex::new(
            r"\[\d{4}-\d{2}-\d{2} \d{2}:(\d{2})\] falls asleep")
            .unwrap();
    }
    if let Some(caps) = BEGIN_RE.captures(line) {
        Event::Begin(caps.get(2).unwrap().as_str().parse::<u32>().unwrap())
    }
    else if let Some(caps) = WAKE_RE.captures(line) {
        Event::Wake(caps.get(1).unwrap().as_str().parse::<usize>().unwrap())
    }
    else if let Some(caps) = SLEEP_RE.captures(line) {
        Event::Sleep(caps.get(1).unwrap().as_str().parse::<usize>().unwrap())
    }
    else {
        panic!("{:?} didn't match!", line)
    }
}
fn parse_input() -> Vec<Event> {
    let mut lines = include_str!("../../input.txt")
                    .lines()
                    .collect::<Vec<&'static str>>();
    lines.sort();
    lines
    .into_iter()
    .map(|l| parse_line(l))
    .collect()
}

fn convert_to_hours(events: Vec<Event>) -> Vec<Hour> {
    let mut out = vec![];
    let mut sleep_minute: Option<usize> = None;
    // make a new hour
    let (first, others) = events.split_at(1);

    if let Event::Begin(id) = first[0] {
        let new_hour = Hour {guard: id,
                             sleep: [false; 60]};
        out.push(new_hour);
    }
    else {
        panic!("First event has to be new guard")
    }
    for event in others {
        match event {
            Event::Sleep(min) => {
                sleep_minute = Some(*min);
            }
            Event::Wake(min) => {
                for i in sleep_minute.unwrap()..*min {
                    out.last_mut().unwrap().sleep[i] = true;
                }
                sleep_minute = None;
            }
            Event::Begin(id) => {
                if let Some(min) = sleep_minute {
                    for i in min..60 {
                        out.last_mut().unwrap().sleep[i] = true;
                    }
                }
                let new_hour = Hour {guard: *id,
                                     sleep: [false; 60]};
                sleep_minute = None;
                out.push(new_hour);
            }
        }
    }
    out
}

fn main() {
    let events = parse_input();
    let hours = convert_to_hours(events);
    let mut guard_sleep_mins: HashMap<u32, [u32; 60]> = HashMap::new();
    let mut guard_sleep_total: HashMap<u32, u32> = HashMap::new();
    for hour in hours.into_iter() {
        let mins_entry = guard_sleep_mins.entry(hour.guard).or_insert_with(|| [0u32; 60]);
        let total_entry = guard_sleep_total.entry(hour.guard).or_insert_with(|| 0u32);
        for i in 0..60 {
            if hour.sleep[i] {
                (*mins_entry)[i] += 1;
                *total_entry += 1;

            }
        }
    }
    let sleepiest_guard = guard_sleep_total
                          .iter()
                          .max_by_key(|(&_k,&v)| v)
                          .map(|(&k,&_v)| k)
                          .unwrap();
    println!("Sleepy guard is {:?}", sleepiest_guard);
    let min_most_asleep = guard_sleep_mins
                          .get(&sleepiest_guard)
                          .unwrap()
                          .iter()
                          .enumerate()
                          .max_by_key(|(_, &sleep)| sleep)
                          .map(|(i, &_)| i)
                          .unwrap();
    println!("{:?}", (sleepiest_guard as usize) * min_most_asleep );

}
